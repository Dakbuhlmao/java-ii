public class Board{
  private Die d1;
  private Die d2;
  private boolean[] closedTiles;
  public Board() {
    this.d1 = new Die();
    this.d2 = new Die();
    this.closedTiles = new boolean[12];
  }
  public String toString() {
    String mt = "";
    for (int i = 0; i < this.closedTiles.length; i++){
      if(this.closedTiles[i] == true){
        mt = mt + "X ";
      }
      else {
        mt = mt + (i + 1) + " ";
      }
    }
    return mt;
  }
  public boolean playATurn(){
    d1.roll();
    d2.roll();
    System.out.println(d1.getPips() + d2.getPips());
    int sumPips = d1.getPips() + d2.getPips();
    if(closedTiles[sumPips - 1] == false){
      System.out.println("Closing tile: " + sumPips);
      closedTiles[sumPips - 1] = true;
      return false;
    }
    else{
      System.out.println("Tile at this position is closed :P");
      return true;
    }
  }
}
