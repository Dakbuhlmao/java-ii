import java.util.Random;
public class Die {
  private int pips;
  private Random r;
  public Die(){
    this.pips = 1;
    this.r = new Random();
  }
  public int getPips() {
    return this.pips;
  }
  public Random getR() {
    return this.r;
  }
  public void roll() {
   this.pips = r.nextInt(5) + 1;
  } 
  public String toString() {
    return "Pips is: " + this.pips;
  }
}
