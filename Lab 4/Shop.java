import java.util.*;
public class Shop{ 
  public static void main(String[] args){
   
    Toy[] shop = new Toy[4];
    Scanner reader = new Scanner(System.in);

    for (int i = 0; i < shop.length; i++){
      System.out.println("Hero Name:");
      String heroName = reader.nextLine();
      System.out.println("Price:");
      double price = Double.parseDouble(reader.nextLine());
      System.out.println("Accessories?");
      Boolean accessories = Boolean.parseBoolean(reader.nextLine());
      shop[i] = new Toy(heroName,price,accessories);
    }
    //setter
    System.out.println("The last toy's name is: " + shop[shop.length-1].getHeroName());
    System.out.println("The last toy's price is: " + shop[shop.length-1].getPrice());
    System.out.println(shop[shop.length-1].getAccessories());
    if (shop[shop.length-1].getAccessories() == true)
    {
      System.out.println("You added the extra accessories on the last toy");
    }
    else 
    {
      System.out.println("No extra accessories added on the last toy");
    }
    shop[shop.length - 1].setHeroName("hero");
    shop[shop.length - 1].setPrice(40);
    System.out.println("The last toy's new name is: " + shop[shop.length-1].getHeroName());
    System.out.println("The last toy's new price is: " + shop[shop.length-1].getPrice());
    Toy kamenRider = new Toy("Decade",24,true);
    kamenRider.priceAfterTax(shop[shop.length-1].getPrice() , shop[shop.length-1].getHeroName(), shop[shop.length-1].getAccessories());
  }
}