public class Toy {
  private String heroName;
  private double price;
  private boolean accessories;
  
  public Toy (String heroName,double price, boolean accessories){
    this.price = price;
    this.heroName = heroName;
    this.accessories = accessories;
  }
  //getter
  public String getHeroName(){
    return this.heroName;
  }
  public double getPrice(){
    return this.price;
  }
  public boolean getAccessories(){
    return this.accessories;
  }
  //setter
  public void setHeroName(String heroNameInput){
    this.heroName = heroNameInput;
  }
  public void setPrice(double priceInput){
    this.price = priceInput;
  }

  
  public void priceAfterTax (double x, String y, boolean z)
  {
    if (this.accessories = false)
    {
      double finalPrice = x * 1.15;
      System.out.println("The price of " + y + " is " + finalPrice +"$");
    }
    else 
    {
      double finalPrice = (x + 5) * 1.15;
      System.out.println("The price of " + y + " is " + finalPrice +"$");

    }
  }
}