import java.util.*;
public class PartThree 
{
    public static void main (String[] args){
        Scanner reader = new Scanner(System.in);
        int x = reader.nextInt();
        int y = reader.nextInt();
        int z = reader.nextInt();
        AreaComputations ac = new AreaComputations();
        System.out.println(ac.areaSquare(x));
        System.out.println(ac.areaRectangle(y,z));
    }   
}