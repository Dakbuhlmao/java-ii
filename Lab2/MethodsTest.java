public class MethodsTest 
{
  public static void main(String[] args)
  {
    int x = 10;
    System.out.println(x);
    methodNoInputNoReturn();
    System.out.println(x);
    methodOneInputNoReturn(x+50);
   methodTwoInputNoReturn(5,4.3);
   int z = methodNoInputReturnInt();
   System.out.println(z);
   System.out.println(sumSquareRoot(4, 5));
   String s1 = "hello";
   String s2 = "goodbye";
   int y = s1.length();
   System.out.println(y);
   SecondClass secondClass = new SecondClass();
   int a = secondClass.addOne(50);
   System.out.println(a);
   int b = secondClass.addTwo(50);
   System.out.println(b);
  }
  public static void methodNoInputNoReturn()
  {
    System.out.println("I'm in a method that takes no input and return nothing");
    int x = 50;
    System.out.println(x);
  }
  public static void methodOneInputNoReturn(int x)
  {
    System.out.println("Inside the method one input no return");
    System.out.println(x);
  }
  public static void methodTwoInputNoReturn(int x , double y)
  {
    System.out.println("I'm in a method that takes tw input and return nothing");
 System.out.println(x + "," + y);
  }
  public static int methodNoInputReturnInt()
  {
   return 6;
  }
  public static double sumSquareRoot(int x, int y)
  {
   int z = x + y;
   return Math.sqrt(z);
  }
}