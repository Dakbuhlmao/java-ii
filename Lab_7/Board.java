public class Board{
  private Die d1;
  private Die d2;
  private int[][] closedTiles;
  public Board() {
    this.d1 = new Die();
    this.d2 = new Die();
    this.closedTiles = new int[6][6];
    for(int i = 0; i < this.closedTiles.length; i++){
      for(int j = 0; j < this.closedTiles[i].length; j++){
        this.closedTiles[i][j] = 0;
      }
    }
  }
  public String toString() {
    String mt = "";
    for (int i = 0; i < this.closedTiles.length; i++){
      for (int j = 0; j < this.closedTiles[i].length; j++){
        mt = mt + this.closedTiles[i][j] + " ";
      }
      mt = mt + "\n";
    }
    return mt;
  }
  public boolean playATurn(){
    d1.roll();
    d2.roll();
    System.out.println(d1 + " " + d2);
    if(closedTiles[d1.getPips()][d2.getPips()] < 3){
      System.out.println("Closing tile");
      this.closedTiles[d1.getPips()][d2.getPips()] = this.closedTiles[d1.getPips()][d2.getPips()] + 1;
      if (closedTiles[d1.getPips()][d2.getPips()] == 3){
        return true;
      }
      return false;
    }
    else{
      System.out.println("Tile at this position is closed :P");
      return true;
    }
  }
}
